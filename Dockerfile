FROM debian:9-slim
MAINTAINER Philipp Matthaeus <phi@phimath.de>
ENV DEBIAN_FRONTEND noninteractive

RUN apt update -q && apt install -qy wget texlive-full python3 python-pygments gnuplot make git && rm -rf /var/lib/apt/lists/*
RUN wget -O /usr/bin/latexrun https://raw.githubusercontent.com/aclements/latexrun/master/latexrun && chmod a+x /usr/bin/latexrun

WORKDIR /data
VOLUME ["/data"]